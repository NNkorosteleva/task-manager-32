package ru.tsc.korosteleva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.dto.request.ServerAboutRequest;
import ru.tsc.korosteleva.tm.dto.request.ServerVersionRequest;
import ru.tsc.korosteleva.tm.dto.response.ServerAboutResponse;
import ru.tsc.korosteleva.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
