package ru.tsc.korosteleva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.dto.request.AbstractRequest;
import ru.tsc.korosteleva.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(@NotNull RQ request);

}

