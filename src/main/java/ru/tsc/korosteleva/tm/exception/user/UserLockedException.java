package ru.tsc.korosteleva.tm.exception.user;

public final class UserLockedException extends AbstractUserException {

    public UserLockedException() {
        super("Error! User is locked.");
    }

}
