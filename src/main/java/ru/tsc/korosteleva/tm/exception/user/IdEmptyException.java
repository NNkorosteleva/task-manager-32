package ru.tsc.korosteleva.tm.exception.user;

public final class IdEmptyException extends AbstractUserException {

    public IdEmptyException() {
        super("Error! Id is empty.");
    }

}
