package ru.tsc.korosteleva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractUserOwnedModel {

    private static final long serialVersionUID = 1;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

    @Override
    public String toString() {
        return "id='" + getId() + " : " +
                ", login='" + login + " : " +
                ", email='" + email + " : " +
                ", firstName='" + firstName + " : " +
                ", lastName='" + lastName + " : " +
                ", middleName='" + middleName + " : " +
                ", role=" + role;
    }

}
