package ru.tsc.korosteleva.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServerAboutResponse extends AbstractResponse {

    private String name;

    private String email;

}
