package ru.tsc.korosteleva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.korosteleva.tm.api.service.IPropertyService;
import ru.tsc.korosteleva.tm.api.service.IServiceLocator;
import ru.tsc.korosteleva.tm.dto.request.ServerAboutRequest;
import ru.tsc.korosteleva.tm.dto.request.ServerVersionRequest;
import ru.tsc.korosteleva.tm.dto.response.ServerAboutResponse;
import ru.tsc.korosteleva.tm.dto.response.ServerVersionResponse;

public class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
